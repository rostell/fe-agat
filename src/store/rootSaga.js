import { all, fork } from 'redux-saga/effects';

import routerSaga from './modules/router/saga';
import objectsSectionsSaga from './modules/objectsSections/saga';
import gridSaga from './modules/grid/saga';
import gridFilterSaga from './modules/gridFilter/saga';
import objectFormSaga from './modules/objectForm/saga';

export default function* rootSaga() {
  yield all([
    fork(routerSaga),
    fork(objectsSectionsSaga),
    fork(gridSaga),
    fork(gridFilterSaga),
    fork(objectFormSaga),
  ]);
}
