import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import * as navigationService from '../services/navigatonService';

import objectsSections from './modules/objectsSections/reducer';
import grid from './modules/grid/reducer';
import gridFilter from './modules/gridFilter/reducer';
import confirmation from './modules/confirmation/reducer';
import objectForm from './modules/objectForm/reducer';

const rootReducer = combineReducers({
  grid,
  gridFilter,
  objectsSections,
  confirmation,
  objectForm,
  router: connectRouter(navigationService.getHistoryInstance()),
});

export default rootReducer;
