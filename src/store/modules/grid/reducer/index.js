// @flow

import type { Reducer } from 'redux';
import { type TableSorting } from '@telsystems/table';
import { type GridColumn } from '../../../../common/models/gridColumn';

import { appConfig } from '../../../../services/configService';

import {
  types as objectsSectionsTypes,
  type Action as objectsSectionsAction,
} from '../../../actions/objectsSectionsMenu';

import {
  types as gridFilterTypes,
  type Action as grigFilterAction,
} from '../../../actions/gridFilter';

import {
  types as gridTypes,
  type Action as GridAction,
} from '../../../actions/grid';

type Action = objectsSectionsAction | GridAction | grigFilterAction;

export type State = {|
  +gridColumns: Array<GridColumn>,
  +items: Array<Object>,
  +selectedIndexes: Array<number>,
  +isItemsLoading: boolean,
  +sorting: ?TableSorting,
|};

const initialState: State = {
  gridColumns: [],
  items: [],
  selectedIndexes: [],
  isItemsLoading: false,
  sorting: { id: 'id', direction: 'asc' },
};

const reducer: Reducer<State, Action> = (state = initialState, action) => {
  switch (action.type) {
    case objectsSectionsTypes.SET_SELECTED: return {
      ...state,
      gridColumns: getColumnsConfig(action.key),
      items: [],
      isItemsLoading: true,
      selectedIndexes: [],
    };

    case gridFilterTypes.UPDATE_VALUES: return { ...state, selectedIndexes: [] };
    case gridFilterTypes.CLEAR_VALUES: return { ...state, selectedIndexes: [] };

    case gridTypes.SET_ITEMS: return {
      ...state,
      items: action.items,
      isItemsLoading: false,
    };

    case gridTypes.SET_SELECTED: return {
      ...state,
      selectedIndexes: action.indexes,
    };

    case gridTypes.SET_SORTING: return {
      ...state,
      sorting: action.sorting,
      selectedIndexes: [],
    };

    case gridTypes.DELETE_SELECTED: return { ...state };

    default: return state;
  }
};

const getColumnsConfig = (sectionKey: string) => {
  const objectsSection = appConfig.getObjectConfigById(sectionKey);
  if (!objectsSection) return [];
  return objectsSection.grid.columns;
};


export default reducer;
