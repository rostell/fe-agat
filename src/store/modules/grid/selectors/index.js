import { createSelector } from 'reselect';

import * as listModel from '../../../../common/models/common/list';

const getItems = state => state.grid.items;
const getSorting = state => state.grid.sorting;
const getFilterValues = state => state.gridFilter.values;

const getfilteredItems = createSelector(
  getItems,
  getFilterValues,
  listModel.filterItems,
);

export const getFilteresSortedItems = createSelector(
  getfilteredItems,
  getSorting,
  listModel.sortItems,
);
