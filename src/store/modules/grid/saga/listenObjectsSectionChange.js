import { takeLatest, put, call } from 'redux-saga/effects';

import { types as sectionsMenuTypes } from '../../../actions/objectsSectionsMenu';
import { actions as gridActions } from '../../../actions/grid';

import { appConfig } from '../../../../services/configService';
import * as apiService from '../../../../services/apiService';
import { showError } from '../../../../services/notificationService';

export function* updateItemsForObjectsSection(action) {
  const objectsSectionConfig = appConfig.getObjectConfigById(action.key);
  if (!objectsSectionConfig) return;

  try {
    const res = yield call(
      apiService.loadGridItems,
      objectsSectionConfig.grid.loadItemsUrl,
    );
    yield put(gridActions.setItems(res));
  } catch (err) {
    console.log(err); showError(err);
    yield put(gridActions.setItems([]));
  }
}

export default function* listenObjectsSectionChange() {
  yield takeLatest(sectionsMenuTypes.SET_SELECTED, updateItemsForObjectsSection);
}
