import { takeLeading, put, take, select, call } from 'redux-saga/effects';

import * as apiService from '../../../../services/apiService';
import { appConfig } from '../../../../services/configService';
import { showError } from '../../../../services/notificationService';

import { getFilteresSortedItems } from '../selectors';

import { types as gridTypes } from '../../../actions/grid';
import { actions as sectionsActions } from '../../../actions/objectsSectionsMenu';
import {
  types as confirmationTypes,
  actions as confirmationActions,
} from '../../../actions/confirmation';

function* handleObjectsDelete() {
  const state = yield select();
  const {
    grid: { selectedIndexes },
    objectsSections: { selected },
  } = state;

  const currentObjectsSectionsConfig = appConfig.getObjectConfigById(selected);
  if (!currentObjectsSectionsConfig) return;

  const { actions: { delete: {
    requiresConfirmation,
    confirmationText,
    urlTemplate,
  } } } = currentObjectsSectionsConfig;

  if (requiresConfirmation) {
    yield put(confirmationActions.askForConfirmation(confirmationText));
    const { type } = yield take([confirmationTypes.CONFIRM, confirmationTypes.DECLINE]);
    if (type === confirmationTypes.DECLINE) return;
  }
  const filteredSortedItem = getFilteresSortedItems(state);
  const selectedItems = selectedIndexes.map(index => filteredSortedItem[index]);

  try {
    yield call(apiService.deleteObjects, urlTemplate, selectedItems);
  } catch (err) {
    console.log(err);
    showError(err);
  } finally {
    yield put(sectionsActions.setSelected(selected));
  }
}

export default function* listenObjectsDelete() {
  yield takeLeading(gridTypes.DELETE_SELECTED, handleObjectsDelete);
}
