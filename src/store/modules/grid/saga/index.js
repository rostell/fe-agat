import { all, fork } from 'redux-saga/effects';

import listenObjectsSectionChange from './listenObjectsSectionChange';
import listenObjectsDelete from './listenObjectsDelete';

export default function* gridSaga() {
  yield all([
    fork(listenObjectsSectionChange),
    fork(listenObjectsDelete),
  ]);
}
