import { all, fork } from 'redux-saga/effects';

import listenValuesUpdate from './listenValuesUpdate';

export default function* gridFilterSaga() {
  yield all([
    fork(listenValuesUpdate),
  ]);
}
