import { takeEvery, put } from 'redux-saga/effects';
import { push } from 'connected-react-router';
import qs from 'qs';

import { types as gridFilterTypes } from '../../../actions/gridFilter';

function* handleValuesChange(action) {
  const searchStr = qs.stringify(action.values);
  yield put(push({ search: searchStr }));
}

export default function* listenValuesChange() {
  yield takeEvery(
    [gridFilterTypes.UPDATE_VALUES, gridFilterTypes.CLEAR_VALUES],
    handleValuesChange,
  );
}
