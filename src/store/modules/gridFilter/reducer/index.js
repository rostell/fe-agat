// @flow
import { type Reducer } from 'redux';
import qs from 'qs';

import { appConfig } from '../../../../services/configService';

import {
  types as gridFilterTypes,
  type Action as GridFilterAction,
} from '../../../actions/gridFilter';

import {
  types as sectionsTypes,
  type Action as SectionsAction,
} from '../../../actions/objectsSectionsMenu';

import {
  types as routerTypes,
  type Action as RouterAction,
} from '../../../actions/router';

type Action = GridFilterAction | SectionsAction | RouterAction;

type State = {|
  +valuesConfig: Array<{ key: string, title: string }>,
  +values: { [string]: string },
|};

const initialState = {
  valuesConfig: [],
  values: {},
};

const reducer: Reducer<State, Action> = (state = initialState, action) => {
  switch (action.type) {
    case gridFilterTypes.UPDATE_VALUES: return { ...state, values: action.values };
    case gridFilterTypes.CLEAR_VALUES: return { ...state, values: {} };
    case sectionsTypes.SET_SELECTED: return {
      ...state,
      valuesConfig: genValuesConfigBySectionKey(action.key),
    };
    case routerTypes.LOCATION_CHANGE: return {
      ...state,
      values: qs.parse(action.payload.location.search.slice(1)),
    };
    default: return state;
  }
};

const genValuesConfigBySectionKey = (sectionKey) => {
  const sectionConfig = appConfig.getObjectConfigById(sectionKey);
  if (!sectionConfig) return [];
  return sectionConfig.grid.columns
    .filter(col => col.filter)
    .map(({ key, title }) => ({ key, title }));
};

export default reducer;
