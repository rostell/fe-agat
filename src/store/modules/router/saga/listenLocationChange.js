import { takeEvery, put } from 'redux-saga/effects';
import { last } from 'ramda';

import { types as routerTypes } from '../../../actions/router';
import { actions as objectsSectionsActions } from '../../../actions/objectsSectionsMenu';

function* handleLocationChnage(action) {
  const objectsSectionKey = last(action.payload.location.pathname.split('/'));
  if (!objectsSectionKey) return;
  yield put(objectsSectionsActions.setSelected(objectsSectionKey));
}

export default function* listenLocationChange() {
  yield takeEvery(routerTypes.LOCATION_CHANGE, handleLocationChnage);
}
