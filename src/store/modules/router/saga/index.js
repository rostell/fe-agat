import { all, fork } from 'redux-saga/effects';

import listemLocationChange from './listenLocationChange';

export default function* routerSaga() {
  yield all([
    fork(listemLocationChange),
  ]);
}
