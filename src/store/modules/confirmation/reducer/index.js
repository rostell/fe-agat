// @flow

import { type Reducer } from 'redux';

import {
  types as confirmationTypes,
  type Action as ConfirmationAction,
} from '../../../actions/confirmation';

type Action = ConfirmationAction;

type State = {|
  +text: string,
|};

const initialState = {
  text: '',
};

const reducer: Reducer<State, Action> = (state = initialState, action) => {
  switch (action.type) {
    case confirmationTypes.ASK_FOR_CONFIRMATION: return { ...state, text: action.text };
    case confirmationTypes.CONFIRM: return { ...state, text: '' };
    case confirmationTypes.DECLINE: return { ...state, text: '' };
    default: return state;
  }
};

export default reducer;
