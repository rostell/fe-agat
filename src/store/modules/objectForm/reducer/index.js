// @flow
import { type Reducer } from 'redux';
import { isEmpty, dissoc } from 'ramda';

import { type FormConfig } from '../../../../common/models/formConfig';
import { type ObjectData } from '../../../../common/models/objectData';
import { getFieldsVisibilityDiff } from '../../../../common/models/fieldsConfig';

import {
  types as objectFormTypes,
  type Action as ObjectFormAction,
} from '../../../actions/objectForm';

import {
  types as objectsSectionsTypes,
  type Action as ObjectsSectionsAction,
} from '../../../actions/objectsSectionsMenu';

type Action = ObjectFormAction | ObjectsSectionsAction

type State = {
  +formConfig: ?FormConfig,
  +isFormConfigLoading: boolean,
  +isSavingInProgress: boolean,
  +isDeletingInProgress: boolean,
  +fieldsVisibility: { [string]: boolean },
  +fieldsWithError: { [string]: boolean },
  +objectData: ?ObjectData,
};

const initialState = {
  formConfig: null,
  isFormConfigLoading: false,
  isSavingInProgress: false,
  isDeletingInProgress: false,
  fieldsVisibility: {},
  fieldsWithError: {},
  objectData: null,
};

const reducer: Reducer<State, Action> = (state = initialState, action) => {
  switch (action.type) {
    case objectsSectionsTypes.SET_SELECTED: return {
      ...state,
      isFieldsConfigLoading: false,
      formConfig: null,
    };

    case objectFormTypes.SET_OBJECT_DATA: return getStateAfterSetObjectData(state, action);

    case objectFormTypes.UPDATE_OBJECT_FIELD: return {
      ...state,
      objectData: { ...state.objectData, [action.fieldName]: action.value },
      fieldsWithError: dissoc(action.fieldName, state.fieldsWithError),
    };

    case objectFormTypes.SET_FORM_CONFIG: return {
      ...state,
      isFormConfigLoading: false,
      formConfig: action.formConfig,
    };

    case objectFormTypes.FIELD_LOST_FOCUS: return {
      ...state,
      fieldsVisibility: {
        ...state.fieldsVisibility,
        ...getFieldsVisibilityDiff(
          state.formConfig ? state.formConfig.fields : [],
          state.objectData || {},
          action.fieldName,
        ),
      },
    };

    case objectFormTypes.SET_FIELDS_WITH_ERROR: return {
      ...state,
      fieldsWithError: action.fieldsWithError,
    };

    case objectFormTypes.CREATE_NEW_OBJECT: return { ...state, objectData: {} };

    case objectFormTypes.EDIT_OBJECT: return { ...state, objectData: {} };

    case objectFormTypes.SAVE_OBJECT: return { ...state, isSavingInProgress: true };

    default: return state;
  }
};


const getStateAfterSetObjectData = (state, action) => {
  const fieldsVisibilityDiff = state.formConfig && action.objectData && !isEmpty(action.objectData)
    ? getFieldsVisibilityDiff(state.formConfig.fields, action.objectData)
    : {};
  return {
    ...state,
    objectData: action.objectData,
    fieldsVisibility: fieldsVisibilityDiff,
    isSavingInProgress: false,
  };
};

export default reducer;
