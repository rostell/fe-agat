import { takeLeading, select, put, call } from 'redux-saga/effects';

import * as apiService from '../../../../services/apiService';
import { showError } from '../../../../services/notificationService';

import {
  types as objectFormTypes,
  actions as objectFormActions,
} from '../../../actions/objectForm';


function* handleObjectDelete() {
  const { objectForm: { objectData, formConfig } } = yield select();
  const { requestTemplate } = formConfig.actions.delete;
  try {
    yield call(apiService.deleteObjects, requestTemplate, [objectData]);
    yield put(objectFormActions.setObjectData(null));
  } catch (err) {
    console.log(err);
    showError(err);
  }
}

export default function* () {
  yield takeLeading(objectFormTypes.DELETE_OBJECT, handleObjectDelete);
}
