import { all, fork } from 'redux-saga/effects';

import listenObjectCreate from './listenObjectCreate';
import listenObjectEdit from './listenObjectEdit';
import listenObjectSave from './listenObjectSave';
import listenObjectSaveAsNew from './listenObjectSaveAsNew';
import listenObjectDelete from './listenObjectDelete';
import listenObjectFieldUpdate from './listenObjectFieldUpdate';

export default function* objectFormSaga() {
  yield all([
    fork(listenObjectCreate),
    fork(listenObjectEdit),
    fork(listenObjectSave),
    fork(listenObjectSaveAsNew),
    fork(listenObjectDelete),
    fork(listenObjectFieldUpdate),
  ]);
}
