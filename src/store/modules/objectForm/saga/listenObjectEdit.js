import { takeLeading, select, call, all, put } from 'redux-saga/effects';

import { appConfig } from '../../../../services/configService';
import * as apiService from '../../../../services/apiService';
import { showError } from '../../../../services/notificationService';

import {
  types as objectFormTypes,
  actions as objectFormActions,
} from '../../../actions/objectForm';


import updateFormConfig from './utils/updateFormConfig';

function* handleObjectEdit(action) {
  const { objectsSections: { selected } } = yield select();

  const sectionConfig = appConfig.getObjectConfigById(selected);
  const { loadItemUrlTemplate } = sectionConfig.grid;

  try {
    const [[objectData]] = yield all([
      call(apiService.loadObjectForEdit, loadItemUrlTemplate, action.objectData.id),
      yield updateFormConfig({ actionName: 'edit' }),
    ]);
    yield put(objectFormActions.setObjectData(objectData));
  } catch (err) {
    console.log(err); showError(err);
    yield put(objectFormActions.setObjectData(null));
  }
}

export default function* () {
  yield takeLeading(objectFormTypes.EDIT_OBJECT, handleObjectEdit);
}
