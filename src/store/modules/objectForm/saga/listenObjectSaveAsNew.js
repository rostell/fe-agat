import { takeLeading, select, call, put } from 'redux-saga/effects';

import * as apiService from '../../../../services/apiService';
import { showError } from '../../../../services/notificationService';

import {
  types as objectFormTypes,
  actions as objectFormActions,
} from '../../../actions/objectForm';

import validateFields from './utils/validateFields';

function* handleObjectSaveAsNew() {
  const { objectForm: { objectData, formConfig } } = yield select();
  const { requestTemplate } = formConfig.actions.saveAsNew;
  try {
    yield validateFields();
    yield call(apiService.saveObject, requestTemplate, objectData);
    yield put(objectFormActions.setObjectData(null));
  } catch (err) {
    console.log(err); showError(err);
  }
}

export default function* () {
  yield takeLeading(objectFormTypes.SAVE_OBJECT_AS_NEW, handleObjectSaveAsNew);
}
