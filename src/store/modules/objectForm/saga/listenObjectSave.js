import { takeLeading, select, call, put } from 'redux-saga/effects';

import * as apiService from '../../../../services/apiService';
import { showError } from '../../../../services/notificationService';

import validateFields from './utils/validateFields';

import {
  types as objectFormTypes,
  actions as objectFormActions,
} from '../../../actions/objectForm';

function* handleObjectSave() {
  const { objectForm: { objectData, formConfig } } = yield select();
  const { requestTemplate } = formConfig.actions.save;
  try {
    yield validateFields();
    yield call(apiService.saveObject, requestTemplate, objectData);
    yield put(objectFormActions.setObjectData(null));
  } catch (err) {
    console.log(err); showError(err);
  }
}

export default function* () {
  yield takeLeading(objectFormTypes.SAVE_OBJECT, handleObjectSave);
}
