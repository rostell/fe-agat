import { takeLatest, put, select, call } from 'redux-saga/effects';
import { reduce, path } from 'ramda';

import updateFormConfig from './utils/updateFormConfig';
import getFieldsWithUpdatedItems from './utils/getFieldsWithUpdatedItems';

import {
  types as objectFormTypes,
  actions as objectFormActions,
} from '../../../actions/objectForm';

const getFieldInitialValue = (field) => {
  switch (field.viewtype) {
    case 'select': return undefined;
    default: return '';
  }
};

const getFieldShoudUpdateItems = path(['reference', 'reRead', 'open']);

function* handleObjectCreate() {
  yield updateFormConfig({ actionName: 'create' });

  const { objectForm: { formConfig } } = yield select();

  const fieldsWithUpdatedItems = yield call(
    getFieldsWithUpdatedItems,
    formConfig.fields,
    getFieldShoudUpdateItems,
  );

  const objectData = reduce((acc, field) => ({
    ...acc,
    [field.key]: getFieldInitialValue(field),
  }), {}, formConfig.fields);

  yield put(objectFormActions.setObjectData(objectData));
  yield put(objectFormActions.setFormConfig({ ...formConfig, fields: fieldsWithUpdatedItems }));
}


export default function* listenObjectsSectionSelect() {
  yield takeLatest(objectFormTypes.CREATE_NEW_OBJECT, handleObjectCreate);
}
