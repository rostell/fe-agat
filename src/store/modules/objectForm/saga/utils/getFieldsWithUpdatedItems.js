import { mergeDeepRight } from 'ramda';

import * as apiService from '../../../../../services/apiService';


const getFieldsWithUpdatedItems = async (fields, getShouldFieldUpdateItems) => Promise.all(
  fields.map(async (field) => {
    if (!getShouldFieldUpdateItems(field)) return field;
    const items = await apiService.loadReferences(field.reference);
    return mergeDeepRight(field, { items: items.map(item => ({ content: item })) });
  }),
);


export default getFieldsWithUpdatedItems;
