import { select, put } from 'redux-saga/effects';

import { actions as objectFormActions } from '../../../../actions/objectForm';

const falidateField = (fieldConfig, fieldValue) => {
  if (fieldConfig.mandatory && !fieldValue) return 'поле не может быть пустым';
  return false;
};

export default function* validateFields() {
  const { objectForm: { formConfig, objectData } } = yield select();

  const fieldsWithError = formConfig.fields
    .map(field => ({
      key: field.key,
      error: falidateField(field, objectData[field.key]),
    }))
    .filter(field => Boolean(field.error));

  const fieldsWithErrorObj = fieldsWithError
    .reduce((acc, field) => ({ ...acc, [field.key]: field.error }), {});

  yield put(objectFormActions.setFieldsWithError(fieldsWithErrorObj));

  if (fieldsWithError.length) throw new Error('Ошибка валидации формы');
}
