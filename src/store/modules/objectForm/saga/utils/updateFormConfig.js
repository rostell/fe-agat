import { call, put, select } from 'redux-saga/effects';

import * as apiService from '../../../../../services/apiService';
import { appConfig } from '../../../../../services/configService';


import { actions as objectFormAction } from '../../../../actions/objectForm';

export default function* updateFormConfig({ actionName }) {
  const { objectsSections: { selected } } = yield select();

  const sectionConfig = appConfig.getObjectConfigById(selected);
  if (!sectionConfig) return;

  const actionConfig = sectionConfig.actions[actionName];
  if (!actionConfig) return;

  const { getMetaUrl, metaBlockName } = actionConfig;
  const newConfig = yield call(apiService.loadFormConfig, getMetaUrl, metaBlockName);
  yield put(objectFormAction.setFormConfig(newConfig));
}
