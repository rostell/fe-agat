import { takeLatest, select, put, call } from 'redux-saga/effects';
import { showError } from '../../../../services/notificationService';

import {
  types as objectFormTypes,
  actions as objectFormActions,
} from '../../../actions/objectForm';

import getFieldsWithUpdatedItems from './utils/getFieldsWithUpdatedItems';
import validateFields from './utils/validateFields';

const isFieldTriggered = fieldName => (field) => {
  if (!field.reference) return false;
  if (
    !field.reference.reRead.changeFields
    || !field.reference.reRead.changeFields.includes(fieldName)
  ) return false;
  return true;
};

function* handleObjectFieldChange(action) {
  const { fieldName } = action;
  const { objectForm: { formConfig } } = yield select();

  try {
    yield validateFields();
    const updatedFields = yield call(
      getFieldsWithUpdatedItems,
      formConfig.fields,
      isFieldTriggered(fieldName),
    );

    const updatedFormConfig = { ...formConfig, fields: updatedFields };
    yield put(objectFormActions.setFormConfig(updatedFormConfig));
  } catch (err) {
    console.log(err); showError(err);
  }
}

export default function* () {
  yield takeLatest(objectFormTypes.UPDATE_OBJECT_FIELD, handleObjectFieldChange);
}
