// @flow

import { filter, map, compose } from 'ramda';
import { createSelector } from 'reselect';

import type { State } from '../reducer';
import type { ObjectsSection } from '../../../../common/models/objectsSection';

type TreeItem = ObjectsSection & { children: Array<ObjectsSection> }
export type ObjectsSectionsTree = Array<TreeItem>


const buildSectionsTree = (items: Array<ObjectsSection>): ObjectsSectionsTree => compose(
  map(objectsSection => ({
    ...objectsSection,
    children: items.filter(i => i.parentKey === objectsSection.key),
  })),
  filter(objectsSection => !objectsSection.parentKey),
)(items);

const getItems = (state: State) => state.items;

export const getSectionsTree = createSelector<any, any, any, any>(
  getItems,
  buildSectionsTree,
);
