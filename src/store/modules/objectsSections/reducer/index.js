// @flow

import type { Reducer } from 'redux';
import { type ObjectsSection } from '../../../../common/models/objectsSection';

import { appConfig } from '../../../../services/configService';
import { types, type Action } from '../../../actions/objectsSectionsMenu';

export type State = {|
  +items: Array<ObjectsSection>,
  +selected: string,
|};

const initialState: State = {
  items: appConfig.getObjectsConfigs(),
  selected: appConfig.getDefaultObjectsSectionKey(),
};

const reducer: Reducer<State, Action> = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_SELECTED: return { ...state, selected: action.key };
    default: return state;
  }
};

export default reducer;
