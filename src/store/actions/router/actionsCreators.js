// @flow

type LocationChangeAction = {
  type: '@@router/LOCATION_CHANGE',
  payload: {
    location: {
      pathname: string,
      search: string,
      hash: string,
      key: string,
    },
    action: 'POP' | 'PUSH',
    isFirstRendering: boolean,
  },
};

export type Action = LocationChangeAction;
