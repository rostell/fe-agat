// @flow

export const SET_FORM_CONFIG = 'objectForm/SET_FORM_CONFIG';
export const SET_OBJECT_DATA = 'objectForm/SET_OBJECT_DATA';
export const UPDATE_OBJECT_FIELD = 'objectForm/UPDATE_OBJECT_FIELD';
export const SET_FIELDS_WITH_ERROR = 'objectForm/SET_FIELDS_WITH_ERROR';

export const CREATE_NEW_OBJECT = 'objectForm/CREATE_NEW_OBJECT';
export const EDIT_OBJECT = 'objectForm/EDIT_OBJECT';

export const FIELD_LOST_FOCUS = 'objectForm/FIELD_LOST_FOCUS';
export const SAVE_OBJECT = 'objectForm/SAVE_OBJECT';
export const SAVE_OBJECT_AS_NEW = 'objectForm/SAVE_OBJECT_AS_NEW';
export const DELETE_OBJECT = 'objectForm/DELETE_OBJECT';
