// @flow

import { type FormConfig } from '../../../common/models/formConfig';
import { type ObjectData, type ObjectFieldValue } from '../../../common/models/objectData';

import * as types from './actionsTypes';

type SetFormConfigAction = {
  type: 'objectForm/SET_FORM_CONFIG',
  formConfig: FormConfig,
};
type SetObjectDataAction = {
  type: 'objectForm/SET_OBJECT_DATA',
  objectData: ?ObjectData,
};
type UpdateObjectFieldAction = {
  type: 'objectForm/UPDATE_OBJECT_FIELD',
  fieldName: string,
  value: ObjectFieldValue,
};
type CreateNewObjectAction = {
  type: 'objectForm/CREATE_NEW_OBJECT',
};
type EditObjectAction = {
  type: 'objectForm/EDIT_OBJECT',
  objectData: ObjectData,
};
type SetFieldsWithErrorAction = {
  type: 'objectForm/SET_FIELDS_WITH_ERROR',
  fieldsWithError: { [string]: boolean }
};

type FieldLostFocusAction = { type: 'objectForm/FIELD_LOST_FOCUS', fieldName: string };
type SaveObjectAction = { type: 'objectForm/SAVE_OBJECT' };
type SaveObjectAsNewAction = { type: 'objectForm/SAVE_OBJECT_AS_NEW' };
type DeleteObjectAction = { type: 'objectForm/DELETE_OBJECT' };


export type Action =
  SetFormConfigAction
  | SetObjectDataAction
  | UpdateObjectFieldAction
  | CreateNewObjectAction
  | EditObjectAction
  | FieldLostFocusAction
  | SaveObjectAction
  | SaveObjectAsNewAction
  | DeleteObjectAction;

export const setFormConfig = (formConfig: FormConfig): SetFormConfigAction => ({
  type: types.SET_FORM_CONFIG,
  formConfig,
});

export const setObjectData = (objectData: ?ObjectData): SetObjectDataAction => ({
  type: types.SET_OBJECT_DATA,
  objectData,
});

export const updateObjectField = (
  fieldName: string,
  value: ObjectFieldValue,
): UpdateObjectFieldAction => ({
  type: types.UPDATE_OBJECT_FIELD,
  fieldName,
  value,
});

export const setFieldsWithError = (
  fieldsWithError: { [string]: boolean },
): SetFieldsWithErrorAction => ({
  type: types.SET_FIELDS_WITH_ERROR,
  fieldsWithError,
});

export const createNewObject = (): CreateNewObjectAction => ({
  type: types.CREATE_NEW_OBJECT,
});

export const editObject = (objectData: ObjectData): EditObjectAction => ({
  type: types.EDIT_OBJECT,
  objectData,
});

export const fieldLostFocus = (fieldName: string): FieldLostFocusAction => ({
  type: types.FIELD_LOST_FOCUS,
  fieldName,
});
export const saveObject = (): SaveObjectAction => ({
  type: types.SAVE_OBJECT,
});
export const saveObjectAsNew = (): SaveObjectAsNewAction => ({
  type: types.SAVE_OBJECT_AS_NEW,
});
export const deleteObject = (): DeleteObjectAction => ({
  type: types.DELETE_OBJECT,
});
