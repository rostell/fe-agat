// @flow

import * as actionsTypes from './actionsTypes';

type AskForConfirmationAction = {
  type: 'confirmation/ASC_FOR_CONFIRMATION';
  text: string,
};

type ConfirmAction = { type: 'confirmation/CONFIRM' };
type DeclineAction = { type: 'confirmation/DECLINE' };

export type Action = AskForConfirmationAction | ConfirmAction | DeclineAction;

export const askForConfirmation = (text: string) => ({
  type: actionsTypes.ASK_FOR_CONFIRMATION,
  text,
});

export const confirm = () => ({
  type: actionsTypes.CONFIRM,
});

export const decline = () => ({
  type: actionsTypes.DECLINE,
});
