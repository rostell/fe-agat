// @flow

export const ASK_FOR_CONFIRMATION = 'confirmation/ASK_FOR_CONFIRMATION';
export const CONFIRM = 'confirmation/CONFIRM';
export const DECLINE = 'confirmation/DECLINE';
