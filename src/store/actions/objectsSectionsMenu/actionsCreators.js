// @flow

import * as types from './actionsTypes';

type SetSelectedAction = {
  type: 'objectsSectionsMenu/SET_SELECTED',
  key: string,
};

export type Action = SetSelectedAction;


export const setSelected = (key: string): SetSelectedAction => ({
  type: types.SET_SELECTED,
  key,
});
