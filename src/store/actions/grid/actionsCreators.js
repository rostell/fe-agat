// @flow
import { type TableSorting } from '@telsystems/table';
import * as types from './actionsTypes';

type SetItemsAction = {
  type: 'grid/SET_ITEMS',
  items: Array<Object>,
};

type SetSelectedAction = {
  type: 'grid/SET_SELECTED',
  indexes: Array<number>,
};

type SetSortingAction = {
  type: 'grid/SET_SORTING',
  sorting: TableSorting,
};

type DeleteItemsAction = {
  type: 'grid/DELETE_SELECTED',
};

export type Action = SetItemsAction | SetSelectedAction | SetSortingAction;

export const setItems = (items: Array<Object>): SetItemsAction => ({
  type: types.SET_ITEMS,
  items,
});

export const setSelected = (indexes: Array<number>): SetSelectedAction => ({
  type: types.SET_SELECTED,
  indexes,
});

export const setSorting = (sorting: TableSorting): SetSortingAction => ({
  type: types.SET_SORTING,
  sorting,
});

export const deleteSelectedItems = (): DeleteItemsAction => ({
  type: types.DELETE_SELECTED,
});
