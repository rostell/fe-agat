// @flow

// const prefix = 'grid';

export const SET_ITEMS = 'grid/SET_ITEMS';
export const SET_SELECTED = 'grid/SET_SELECTED';
export const SET_SORTING = 'grid/SET_SORTING';

export const LOAD_ITEMS = 'grid/LOAD_ITEMS';
export const DELETE_SELECTED = 'grid/DELETE_SELECTED';
