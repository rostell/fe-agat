// @flow

import * as types from './actionsTypes';

type UpdateValuesAction = {
  type: 'gridFilter/UPDATE_VALUES',
  values: { [string]: string },
};

type ClearValuesAction = {
  type: 'gridFilter/CLEAR_VALUES',
};

export type Action = UpdateValuesAction | ClearValuesAction;

export const updateValues = (values: { [string]: string }): UpdateValuesAction => ({
  type: types.UPDATE_VALUES,
  values,
});

export const clearValues = (): ClearValuesAction => ({
  type: types.CLEAR_VALUES,
});
