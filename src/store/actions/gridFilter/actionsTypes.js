// @flow

export const UPDATE_VALUES = 'gridFilter/UPDATE_VALUES';
export const CLEAR_VALUES = 'gridFilter/CLEAR_VALUES';
