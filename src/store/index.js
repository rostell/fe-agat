import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { routerMiddleware } from 'connected-react-router';

import * as navigationService from '../services/navigatonService';

import rootReducer from './rootReducer';
import rootSaga from './rootSaga';

/* eslint-disable no-underscore-dangle */
const composeEnhancers = global.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
/* eslint-enable */

const sagaMiddleware = createSagaMiddleware();
const store = createStore(rootReducer, composeEnhancers(
  applyMiddleware(
    sagaMiddleware,
    routerMiddleware(navigationService.getHistoryInstance()),
  ),
));

sagaMiddleware.run(rootSaga);

export default store;
