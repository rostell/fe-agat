import React from 'react';
import { Provider } from 'react-redux';
import { createGlobalStyle } from 'styled-components';

import store from './store';

import RootRouter from './router';

const GlobalStyle = createGlobalStyle`
  html, body, #root { height: 100%; }
  body {
    font-family: "Roboto", sans-serif;
  }
  a {
    text-decoration: none !important;
  }
  .-table--3R3or- > div:first-child > div:first-child {
    position: relative !important;
  }
  .-type-draggable--1xUaT- > div:first-child {
    position: relative !important;
  }
`;

const App = () => (
  <Provider store={store}>
    <GlobalStyle />
    <RootRouter />
  </Provider>
);

export default App;
