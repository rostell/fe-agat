// @flow

import React, { useRef, useState, useCallback } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { values } from 'ramda';

import { type FieldsConfig } from '../../models/fieldsConfig';

import MenuItem from '../../components/SideMenu/MenuItem';
import Scrollspy from '../../components/ScrollSpy';

const Container = styled.div`
  width: 200px;
  background-color: white;
`;

const getSeparatorsFromFieldsConfig = (fieldsConfig: FieldsConfig) => values(fieldsConfig)
  .filter(field => field.viewtype === 'separator');

type Props = {
  itemsComtainerEl: string,
  fieldsConfig: FieldsConfig,
};

const FormBlocksMenu = (props: Props) => {
  const scrollspy = useRef();
  const [curSection, setCurSection] = useState(null);

  const selectItem = useCallback(
    (key) => { scrollspy.current && scrollspy.current.scrollTo(key); },
    [scrollspy],
  );

  const handleSectionUpdate = useCallback(
    (el) => { el && setCurSection(el.id); },
    [setCurSection],
  );

  const { fieldsConfig, itemsComtainerEl } = props;
  const separators = fieldsConfig
    ? getSeparatorsFromFieldsConfig(fieldsConfig)
    : [];

  return Boolean(separators.length) && (
    <Container>
      <Scrollspy
        ref={scrollspy}
        rootEl={itemsComtainerEl}
        items={separators.map(separatorField => separatorField.key)}
        onUpdate={handleSectionUpdate}
      >
        {separators.map(separator => (
          <MenuItem
            key={separator.key}
            value={separator.key}
            selected={curSection === separator.key}
            onSelect={selectItem}
          >
            {separator.title}
          </MenuItem>
        ))}
      </Scrollspy>
    </Container>
  );
};

const mapStateToProps = ({ objectForm }) => ({
  fieldsConfig: objectForm.formConfig ? objectForm.formConfig : null,
});

export default connect(mapStateToProps)(FormBlocksMenu);
