// @flow

import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import { actions as objectFormActions } from '../../../store/actions/objectForm';

import Modal from '../../components/Modal';
import Text from '../../components/Text';

import FormBlocksMenu from './FormBlocksMenu';
import FormFields from './FormFields';
import ActionBtnsBlock from './ActionBtnsBlock';

const StyledDialog = styled(Modal)`
  width: 600px; 
  min-height: 300px;
`;

const ContentContainer = styled.div`
  width: 100%;
  border-radius: 4px;
  background-color: rgb(249, 249, 251);
`;

const FormContainer = styled.div`
  width: 100%;
  display: flex;
  border-style: solid;
  border-width: 1px 0px 1px 0px;
  border-color: rgb(234, 234, 243);
`;

const TitleContainer = styled.div`
  padding: 24px;
`;

type Props = {
  formTitle: string,
  shouldShow: boolean,
  formConfigLoaded: boolean,
  onClose: () => void,
};

const ObjectForm = (props: Props) => {
  const { formTitle, shouldShow, formConfigLoaded, onClose } = props;
  return (
    <StyledDialog
      modal
      type="modal"
      closable
      show={shouldShow}
      onCloseClick={onClose}
    >
      <ContentContainer>
        {formConfigLoaded && (
          <>
            <TitleContainer>
              <Text size="title">{formTitle}</Text>
            </TitleContainer>

            <FormContainer>
              <FormBlocksMenu itemsComtainerEl="#form-fields-container" />
              <FormFields />
            </FormContainer>

            <ActionBtnsBlock />
          </>
        )}
      </ContentContainer>
    </StyledDialog>
  );
};

const mapStateToProps = ({ objectForm }) => ({
  formTitle: objectForm.formConfig ? objectForm.formConfig.title : '',
  shouldShow: Boolean(objectForm.objectData),
  formConfigLoaded: Boolean(objectForm.formConfig),
  loading: objectForm.isFieldsConfigLoading,
});

const mapDispatchToProps = {
  onClose: () => objectFormActions.setObjectData(null),
};

export default connect(mapStateToProps, mapDispatchToProps)(ObjectForm);
