// @flow

import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import { actions as objectFormActions } from '../../../store/actions/objectForm';

import { type FormActions } from '../../models/formConfig';

import { Button, IconButton } from '../../components/buttons';

const Container = styled.div`
  padding: 16px 24px;
  display: flex;
  justify-content: space-between;
`;

type Props = {
  actionsConfig: FormActions,
  onSave: () => void,
  onSaveAsNew: () => void,
  onDelete: () => void,
};

const ActionBtnsBlock = (props: Props) => {
  const {
    actionsConfig,
    onSave,
    onSaveAsNew,
    onDelete,
  } = props;
  return (
    <Container>
      <div>
        {!actionsConfig.delete.isHidden && (
          <IconButton
            iconId="trash"
            type="secondary"
            onClick={onDelete}
          >
            Удалить
          </IconButton>
        )}
      </div>

      <div>
        {!actionsConfig.saveAsNew.isHidden && (
          <Button
            type="secondary"
            onClick={onSaveAsNew}
          >
            Сохранить как новый
          </Button>
        )}
      </div>

      <div>
        {!actionsConfig.save.isHidden && (
          <Button
            onClick={onSave}
          >
            Сохранить
          </Button>
        )}
      </div>
    </Container>
  );
};

const mapStateToProps = ({ objectForm: { formConfig } }) => ({
  actionsConfig: formConfig ? formConfig.actions : null,
});

const mapDispatchToProps = {
  onSave: objectFormActions.saveObject,
  onSaveAsNew: objectFormActions.saveObjectAsNew,
  onDelete: objectFormActions.deleteObject,
};

export default connect(mapStateToProps, mapDispatchToProps)(ActionBtnsBlock);
