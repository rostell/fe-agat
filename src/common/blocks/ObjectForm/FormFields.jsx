// @flow

import React from 'react';
import styled from 'styled-components';
import { compose, sort, filter } from 'ramda';
import { connect } from 'react-redux';

import { type FormConfig } from '../../models/formConfig';
import { type ObjectData, type ObjectFieldValue } from '../../models/objectData';

import { actions as objectFormActions } from '../../../store/actions/objectForm';

import { Content } from '../../components/Layout';
import FormField from '../../components/form/FormField';
import FormFieldsSeparator from '../../components/form/FieldsSeparator';


const FieldsContainer = styled(Content)`
  max-height: 300px;
  overflow: auto;
`;

const prepareFields = compose(
  sort((field1, field2) => {
    if (field1.order > field2.order) return 1;
    if (field1.order < field2.order) return -1;
    return 0;
  }),
  filter(field => !field.isHidden),
);

type Props = {
  objectData: ObjectData,
  formConfig: FormConfig,
  fieldsVisibility: { [string]: boolean },
  fieldsWithError: { [string]: ?string },
  onFieldChange: (key: string, value: ObjectFieldValue) => void,
  onFieldLostFocus: (fieldName: string) => void,
};

const FormFields = (props: Props) => {
  const {
    objectData,
    formConfig,
    fieldsVisibility,
    fieldsWithError,
    onFieldChange,
    onFieldLostFocus,
  } = props;

  const fields = formConfig ? formConfig.fields : [];

  const preparedFields = prepareFields(fields);

  return (
    <FieldsContainer id="form-fields-container">

      {Boolean(objectData) && preparedFields.map((field, index) => field.viewtype === 'separator' ? (
        <FormFieldsSeparator
          id={field.key}
          key={field.key}
          title={field.title}
        />
      ) : fieldsVisibility[field.key] && (
        <FormField
          index={index}
          key={field.key}
          fieldConfig={field}
          value={objectData[field.key]}
          errorText={fieldsWithError[field.key]}
          onChange={value => onFieldChange(field.key, value)}
          onBlur={() => onFieldLostFocus(field.key)}
        />
      ))}

    </FieldsContainer>
  );
};


const mapStateToProps = ({ objectForm }) => ({
  objectData: objectForm.objectData,
  formConfig: objectForm.formConfig,
  fieldsWithError: objectForm.fieldsWithError,
  fieldsVisibility: objectForm.fieldsVisibility,
});

const mapDispatchToProps = {
  onFieldChange: objectFormActions.updateObjectField,
  onFieldLostFocus: objectFormActions.fieldLostFocus,
};


export default connect(mapStateToProps, mapDispatchToProps)(FormFields);
