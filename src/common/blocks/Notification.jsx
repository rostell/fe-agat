import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { remove } from 'ramda';

import * as notificationService from '../../services/notificationService';

import SnackBar from '../components/SnackBar';

const ErrorNotification = styled(SnackBar)`
  z-index: 9999;
  background-color: ${({ type }) => type === 'error' && 'red'};
  box-shadow: ${({ type }) => type === 'error' && '0 10px 25px 0 rgb(100, 0, 0, 0.5)'};
  top: ${({ index }) => index * 50 + 10}px;
`;

const Notification = () => {
  const [notifications, setNotifications] = useState([]);

  useEffect(() => {
    notificationService.setHandler((notificationConfig) => {
      setNotifications([...notifications, notificationConfig]);
    });
  });

  return (
    <>
      {notifications.map((notification, index) => (
        <ErrorNotification
          key={notification.id}
          index={index}
          show
          type={notification.type}
          text={notification.text}
          onCloseClick={() => {
            const i = notifications.indexOf(notification);
            setNotifications(remove(i, 1, notifications));
          }}
        />
      ))}
    </>
  );
};

export default Notification;
