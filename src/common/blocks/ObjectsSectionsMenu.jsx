// @flow

import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import SideMenu, { MenuHeader, MenuItem, MenuItemsBlock } from '../components/SideMenu';

import { basePath } from '../consts';

import {
  getSectionsTree,
  type ObjectsSectionsTree,
} from '../../store/modules/objectsSections/selectors';

const Container = styled.div`
  padding: 32px 0px 0px 16px;
`;

type Props = {
  objectsSectionsTree: ObjectsSectionsTree,
  selectedKey: string,
};

const formatChild = ({ title, key }) => ({ title, value: key, link: key });

const ObjectsSectionsMenu = (props: Props) => {
  const { objectsSectionsTree, selectedKey } = props;

  return (
    <Container>
      <SideMenu>
        <MenuHeader>Объекты</MenuHeader>
        {objectsSectionsTree.map(item => item.children.length > 0
          ? (
            <MenuItemsBlock
              key={item.key}
              items={item.children.map(formatChild)}
              selectedKey={selectedKey}
            >
              {item.title}
            </MenuItemsBlock>
          ) : (
            <MenuItem
              key={item.key}
              value={item.key}
              link={`${basePath}/${item.key}`}
              selected={item.key === selectedKey}
            >
              {item.title}
            </MenuItem>
          ))
        }
      </SideMenu>
    </Container>
  );
};

const mapStateToProps = ({ objectsSections }) => ({
  objectsSectionsTree: getSectionsTree(objectsSections),
  selectedKey: objectsSections.selected,
});

export default connect(mapStateToProps)(ObjectsSectionsMenu);
