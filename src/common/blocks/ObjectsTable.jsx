// @flow

import React from 'react';

import { connect } from 'react-redux';
import { type TableSorting } from '@telsystems/table';

import { actions as gridActions } from '../../store/actions/grid';
import { actions as objectFormActions } from '../../store/actions/objectForm';
import { getFilteresSortedItems } from '../../store/modules/grid/selectors';

import Table from '../components/tables/Table';

import { type ObjectData } from '../models/objectData';

type Props = {
  columnsConfig: Array<Object>,
  items: Array<ObjectData>,
  sorting: TableSorting,
  selectedIndexes: Array<number>,
  loading: boolean,

  onEdit: (data: ObjectData) => void,
  onSetSorting: (sorting: TableSorting) => void,
  onSetSelected: (indexes: Array<number>) => void,
}

const genSchema = columnsConfig => columnsConfig
  .filter(config => !config.hidden)
  .map(config => ({
    type: config.type,
    title: config.title,
    id: config.key,
    width: 100,
  }));

const ObjectsTable = (props: Props) => {
  const {
    columnsConfig,
    items,
    sorting,
    selectedIndexes,
    loading,

    onEdit,
    onSetSorting,
    onSetSelected,
  } = props;

  const columns = genSchema(columnsConfig);
  return (
    <Table
      componentType={1}
      isWaiting={loading}
      align="left"
      orientation="vertical"
      scheme={columns}
      records={items}
      selected={selectedIndexes}
      sortedBy={sorting}

      onSort={onSetSorting}
      onSelect={onSetSelected}
      onDoubleClick={({ record }) => { if (record) onEdit(record); }}
    />
  );
};

const mapStateToProps = state => ({
  columnsConfig: state.grid.gridColumns,
  items: getFilteresSortedItems(state),
  sorting: state.grid.sorting,
  selectedIndexes: state.grid.selectedIndexes,
  loading: state.grid.isItemsLoading,
});

const mapDispatchToProps = {
  onEdit: objectFormActions.editObject,
  onSetSorting: gridActions.setSorting,
  onSetSelected: gridActions.setSelected,
};

export default connect(mapStateToProps, mapDispatchToProps)(ObjectsTable);
