// @flow

import React, { useState, useCallback, useEffect } from 'react';
import { connect } from 'react-redux';

import { actions as grigFilterActions } from '../../../store/actions/gridFilter';

import DropDown from '../../components/DropDown';
import TextInput from '../../components/TextInput';
import Tooltip from '../../components/Tooltip';

import { genItems, getInputTextFromValues, getValuesFromInputText } from './utils';


type Props = {
  valuesConfig: { [string]: string },
  values: { [string]: string },
  onValuesChange: (values: { [string]: string }) => void,
  onValuesClear: () => void,
};

const GridFilter = (props: Props) => {
  const { valuesConfig, values, onValuesChange, onValuesClear } = props;

  const [inputText, setInputText] = useState(getInputTextFromValues(values));

  const handleItemClick = useCallback(
    item => onValuesChange({ ...values, [item.id]: '' }),
    [values, valuesConfig, onValuesChange],
  );

  const handleInputFocusLost = useCallback(
    () => onValuesChange(getValuesFromInputText(inputText)),
    [inputText, onValuesChange],
  );

  const handleInputChange = useCallback(
    (value) => { if (!value) onValuesClear(); else setInputText(value); },
  );

  useEffect(
    () => setInputText(getInputTextFromValues(values)),
    [values, valuesConfig, onValuesChange],
  );

  const items = genItems(valuesConfig, values);

  return (
    <div>
      <DropDown
        selectedIndex={1}
        items={items}
        position="left"
        onItemClick={handleItemClick}
      >
        <TextInput
          value={inputText}
          placeholder="Поле_1=значение_1, Поле_N=значение_N"
          onBlur={handleInputFocusLost}
          clearable
          onChange={handleInputChange}
        />
      </DropDown>
      <Tooltip
        size="large"
        position="bottom"
        header="Фильтрация"
        text={`
          % - любые символы.
          Поиск ИЛИ: "a;b" означает поиск a или b
          Числа: "~10;50~" означает поиск в диапазоне [-inf, 10] ИЛИ [50, +inf]
        `}
        link="#"
        align="center"
      />
    </div>
  );
};

const mapStateToProps = ({ gridFilter }) => ({
  valuesConfig: gridFilter.valuesConfig,
  values: gridFilter.values,
});

const mapDispatchToProps = {
  onValuesChange: grigFilterActions.updateValues,
  onValuesClear: grigFilterActions.clearValues,
};

export default connect(mapStateToProps, mapDispatchToProps)(GridFilter);
