import { keys, isNil, filter, split, reduce, compose, replace } from 'ramda';

export const genItems = (valuesConfig, values) => valuesConfig
  .filter(({ key }) => isNil(values[key]))
  .map(({ key, title }) => ({ content: {
    id: key,
    title: `${title} / ${key}`,
  } }));

export const getInputTextFromValues = values => keys(values)
  .reduce((acc, key) => `${acc}${acc && ', '}${key}=${values[key]}`, '');

export const getValuesFromInputText = compose(
  reduce((acc, item) => {
    const [key, value = ''] = item.split('=');
    return { ...acc, [key]: value };
  }, {}),
  filter(item => item),
  split(','),
  replace(/ /g, ''),
);
