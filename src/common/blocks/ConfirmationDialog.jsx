// @flow

import React from 'react';
import { connect } from 'react-redux';
import Confirm from '../components/Confirm';

import { actions as confirmationActions } from '../../store/actions/confirmation';

type Props = {
  text: string,
  onConfirm: () => void,
  onDecline: () => void,
};

const ConfirmationDialog = (props: Props) => {
  const { text, onConfirm, onDecline } = props;
  return (
    <Confirm
      type="modal"
      modal
      show={Boolean(text)}
      text={text}
      onConfirmClick={onConfirm}
      onCloseClick={() => { setTimeout(onDecline, 100); }}
    />
  );
};

const mapStateToProps = ({ confirmation }) => ({
  text: confirmation.text,
});

const mapDispatchToProps = {
  onConfirm: confirmationActions.confirm,
  onDecline: confirmationActions.decline,
};

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmationDialog);
