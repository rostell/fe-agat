// @flow

type ColType = 'str' | 'num' | 'bit' | 'date' | 'datetime' | 'time';

export type GridColumn = {
  key: string,
  title: string,
  order: number,
  type: ColType,
  hidden: boolean,
  filter: boolean,
};

export const genGridColumnFromConfig = (columnsConfig: Object): GridColumn => ({
  key: columnsConfig.column_key || '',
  title: columnsConfig.title || '',
  order: columnsConfig.order || 0,
  type: columnsConfig.type || 'str',
  hidden: columnsConfig.hidden || false,
  filter: columnsConfig.filter || false,
});
