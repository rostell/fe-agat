// @flow

import { genGridFromConfig, type Grid } from './grid';


export type ObjectsSection = {
  key: string,
  title: string,
  order: number,
  type: 'full' | 'onlyname',
  hidden: boolean,
  parentKey: ?string,
  grid: Grid,
};

export const genObjectsSectionsFromConfig = (objectConfig: Object): ObjectsSection => ({
  key: objectConfig.object_key,
  title: objectConfig.object_title,
  order: objectConfig.order,
  type: objectConfig.type,
  hidden: objectConfig.type || false,
  parentKey: objectConfig.up_level,
  grid: genGridFromConfig(objectConfig.grid),
  actions: {
    create: {
      title: objectConfig.grid.button_create.title,
      description: objectConfig.grid.button_create.descr,
      getMetaUrl: objectConfig.grid.button_create.metadata,
      metaBlockName: objectConfig.grid.button_create.metadata_block,
      isHidden: objectConfig.grid.button_create.hidden,
    },
    edit: objectConfig.grid.edit_enabled ? {
      getMetaUrl: objectConfig.grid.edit_metadata,
      metaBlockName: objectConfig.grid.edit_metadata_block,
    } : null,
    delete: {
      title: objectConfig.grid.button_delete.title,
      isHidden: objectConfig.grid.button_delete.hidden,
      urlTemplate: objectConfig.grid.button_delete.api_delete,
      requiresConfirmation: objectConfig.grid.button_delete.additional_approve,
      confirmationText: objectConfig.grid.button_delete.text_additional_approve,
    },
  },
});
