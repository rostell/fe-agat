// @flow

import { type FieldsConfig } from './index';
import { type ObjectData } from '../objectData';


const isFieldAffected = (fieldConfig, fieldName) => {
  if (!fieldConfig.conditions) return false;
  return Boolean(
    fieldConfig.conditions.find(condition => condition.fieldName === fieldName),
  );
};

const getAffectedFields = (fieldsConfig, fieldName) => fieldsConfig
  .filter(fieldConfig => isFieldAffected(fieldConfig, fieldName));


const isFieldFitCondition = ({ fieldName, condition }, objectData) => {
  switch (condition.type) {
    case 'is_empty': return !objectData[fieldName];
    case 'is_not_empty': return Boolean(objectData[fieldName]);
    case 'is_equals': return objectData[fieldName] === condition.value;
    case 'is_not_equals': return objectData[fieldName] !== condition.value;
    default: return true;
  }
};

const isFieldFitConditions = (conditionsConfig, objectData) => conditionsConfig
  ? conditionsConfig.reduce((acc, conditionConfig) => {
    return acc && isFieldFitCondition(conditionConfig, objectData);
  }, true)
  : true;

const getFieldsVisibilityDiff = (
  fieldsConfig: FieldsConfig,
  objectData: ObjectData,
  changedFieldName: ?string,
) => {
  const affectedFields = changedFieldName
    ? getAffectedFields(fieldsConfig, changedFieldName)
    : fieldsConfig;

  const fieldsVisibility = affectedFields.reduce((acc, field) => ({
    ...acc,
    [field.key]: isFieldFitConditions(field.conditions, objectData),
  }), {});

  return fieldsVisibility;
};

export default getFieldsVisibilityDiff;
