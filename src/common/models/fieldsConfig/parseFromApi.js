// @flow
import { map } from 'ramda';

import { type FieldConfig, type FieldsConfig } from './index';

const parseReference = referenceRaw => ({
  api: referenceRaw.api,
  value: referenceRaw.value,
  title: referenceRaw.title,
  reRead: {
    open: referenceRaw.re_read.open,
    changeFields: referenceRaw.re_read.change_fields,
  },
  input: referenceRaw.input ? {
    seq: referenceRaw.input.seq,
    value: referenceRaw.input.value,
  } : null,
});

const parseConditionConfig = conditionsConfig => ({
  fieldName: conditionsConfig.fieldName,
  condition: {
    type: conditionsConfig.condition.type,
    value: conditionsConfig.condition.value,
  },
});

export const parseFieldConfigFromApi = (fieldConfigRaw: Object): FieldConfig => ({
  key: fieldConfigRaw.field_key || '',
  title: fieldConfigRaw.title || fieldConfigRaw.field_key || '',
  description: fieldConfigRaw.descr || '',
  type: fieldConfigRaw.type || 'str',
  viewtype: fieldConfigRaw.viewtype || 'str',
  reference: fieldConfigRaw.reference ? parseReference(fieldConfigRaw.reference) : null,

  items: fieldConfigRaw.items ? fieldConfigRaw.items : null,
  readonly: fieldConfigRaw.readonly || false,
  mandatory: fieldConfigRaw.mandatory || false,
  isHidden: fieldConfigRaw.hidden || false,
  default: fieldConfigRaw.default || null,
  order: fieldConfigRaw.order || 1000,
  conditions: fieldConfigRaw.conditions ? fieldConfigRaw.conditions.map(parseConditionConfig) : [],
});

const parseFromApi = (fieldsConfigRaw: Object): FieldsConfig => map(
  parseFieldConfigFromApi,
  fieldsConfigRaw,
);

export default parseFromApi;
