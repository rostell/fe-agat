// @flow

// import { map } from 'ramda';

export type FieldType = 'str' | 'text' | 'int' | 'bit' | 'uuid';
export type ViewType = 'select' | 'separator' | 'str' | 'text' | 'bit' | 'int';

type Reference = {
  api: string,
  value: string,
  title: string,
  reRead: {
    open: boolean,
    changeFields: Array<string>,
  },
  input: ?{
    seq: 'first' | 'last' | 'random',
    value: string,
  }
}

type ConditionsConfig = Array<{
  fieldName: string,
  condition: { type: string, value: ?string },
}>

export type FieldConfig = {
  key: string,
  title: string,
  description: string,
  type: FieldType,
  viewtype: ViewType,
  reference: ?Reference,
  items: ?Array<{content: { title: string, id: string } }>,
  readonly: boolean,
  mandatory: boolean,
  isHidden: boolean,
  default: ?mixed,
  order: number,
  conditions: ConditionsConfig,
};

export type FieldsConfig = Array<FieldConfig>;

export { default as parseFromApi } from './parseFromApi';
export { default as getFieldsVisibilityDiff } from './getFieldsVisibility';
