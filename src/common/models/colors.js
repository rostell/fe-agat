// @flow

export type Colors = {
  primary: string,
  secondary: string,
  dark: string,
  comet: string,
  error: string,
  grey: string,
  light: string,
  paper: string,
};

export const genColorsFromConfig = (colorsConfig: Object): Colors => ({
  primary: colorsConfig.primary || '#c71a1a',
  secondary: colorsConfig.secondary || '#246bd5',
  dark: colorsConfig.dark || '#10162b',
  comet: colorsConfig.comet || '#10162b',
  error: colorsConfig.error || '#f50c0c',
  grey: colorsConfig.grey || '#c0c0ca',
  light: colorsConfig.light || '#f2f2f7',
  paper: colorsConfig.paper || '#FFFFFF',
});
