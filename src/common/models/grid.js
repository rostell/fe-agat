// @flow

import { genGridColumnFromConfig, type GridColumn } from './gridColumn';

export type Grid = {
  columns: Array<GridColumn>,
  loadItemsUrl: string,
};

export const genGridFromConfig = (gridConfig: Object): Grid => ({
  columns: gridConfig.columns.map(genGridColumnFromConfig),
  loadItemsUrl: gridConfig.api_read,
  loadItemUrlTemplate: gridConfig.edit_api,
});
