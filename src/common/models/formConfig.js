// @flow

import {
  parseFromApi as parseFields,
  type FieldsConfig,
} from './fieldsConfig';

export type FormAction = {
  title: string,
  requestTemplate: string,
  isHidden: boolean,
};

export type FormActions = {
  save: FormAction,
  saveAsNew: FormAction,
  delete: FormAction,
};

export type FormConfig = {
  title: string,
  actions: FormActions,
  fields: FieldsConfig,
};

const parseAction = actionConfig => ({
  title: actionConfig.title,
  requestTemplate: actionConfig.api,
  isHidden: actionConfig.hidden,
});

export const parseFromApi = (objectMeta: Object): FormConfig => ({
  title: objectMeta.title,
  actions: {
    save: parseAction(objectMeta.buttons.button_save),
    saveAsNew: parseAction(objectMeta.buttons.button_save_asnew),
    delete: parseAction(objectMeta.buttons.button_delete),
  },
  fields: parseFields(objectMeta.fields),
});
