import { sort } from 'ramda';

const evalCond = (fieldValue1, fieldValue2, direction) => {
  const coef = direction === 'asc' ? 1 : -1;
  if (fieldValue1 > fieldValue2) return coef;
  if (fieldValue1 < fieldValue2) return -coef;
  return 0;
};

export default (items, sorting) => {
  if (!sorting || !sorting.direction) return items;
  return sort((item1, item2) => {
    const fieldValue1 = item1[sorting.id];
    const fieldValue2 = item2[sorting.id];

    return evalCond(fieldValue1, fieldValue2, sorting.direction);
  }, items);
};
