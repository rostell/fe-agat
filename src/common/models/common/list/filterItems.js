import { map, keys } from 'ramda';
import isObject from 'is-object';

const filterEmptyValues = values => keys(values)
  .reduce((acc, key) => values[key] ? ({ ...acc, [key]: values[key] }) : acc, {});

const parseRangeValue = (value) => {
  const [minStr, maxStr] = value.split('~');
  const min = minStr === '' ? -Infinity : Number(minStr);
  const max = maxStr === '' ? Infinity : Number(maxStr);
  return { min, max };
};

const parseFilterValue = (value) => {
  if (value.search(';') !== -1) return value.split(';').map(parseFilterValue);
  if (value.search('~') !== -1) return parseRangeValue(value);
  return value;
};

const isValueInRange = (value, { min, max }) => Number(value) >= min && value <= max;
const isValueEquals = (value, filterValue) => value === filterValue;

const isValueFitCondition = (value, filterValue) => isObject(filterValue)
  ? isValueInRange(value, filterValue)
  : isValueEquals(value, filterValue);

const isValueFitConditions = (value, conditions) => conditions
  .reduce((acc, cond) => acc || isValueFitCondition(value, cond), false);

const isItemFitConditions = (item, filterValues) => {
  const res = keys(filterValues).reduce((acc, key) => {
    const filterValue = filterValues[key];
    const itemValue = item[key];
    if (Array.isArray(filterValue)) return acc && isValueFitConditions(itemValue, filterValue);
    return isValueFitCondition(itemValue, filterValue);
  }, true);
  return res;
};

export default (items, filterValues) => {
  const nonEmptyValues = filterEmptyValues(filterValues);
  const parsedValues = map(parseFilterValue, nonEmptyValues);
  return items.filter(item => isItemFitConditions(item, parsedValues));
};
