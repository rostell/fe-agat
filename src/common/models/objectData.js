// @flow

export type ObjectFieldValue = string | boolean | number;

export type ObjectData = {
  [string]: ObjectFieldValue,
};
