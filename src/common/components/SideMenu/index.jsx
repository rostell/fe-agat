// @flow

import React, { type Node, type ComponentType } from 'react';
import styled from 'styled-components';

const Container: ComponentType<any> = styled.div`
  /* padding: 38px 0 20px 15px; */
  height: 100%;
`;

type Props = {
  children: Array<Node>,
}

const SideMenu = (props: Props) => {
  const { children } = props;
  return (
    <Container>
      {children}
    </Container>
  );
};

export default SideMenu;
export { default as MenuHeader } from './MenuHeader';
export { default as MenuItem } from './MenuItem';
export { default as MenuItemsBlock } from './MenuItemsBlock';
