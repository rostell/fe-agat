// @flow

import React, { Fragment, memo } from 'react';
import MenuItem from '../MenuItem';

type Props = {
  items: Array<{ title: string, value: string, link?: string }>,
  selectedKey: string,
  onSelect: (key: string) => void,
};

const ChildsList = (props: Props) => {
  const { items, onSelect, selectedKey } = props;
  return (
    <Fragment>
      {items.map(item => (
        <MenuItem
          key={item.value}
          value={item.value}
          link={item.link}
          level={1}
          selected={item.value === selectedKey}
          onSelect={onSelect}
        >
          {item.title}
        </MenuItem>
      ))}
    </Fragment>
  );
};

export default memo<Props>(ChildsList);
