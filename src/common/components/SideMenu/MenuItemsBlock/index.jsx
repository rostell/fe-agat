// @flow

import * as React from 'react';
import Collapse from 'react-collapse';
import styled from 'styled-components';

import MenuItem from '../MenuItem';
import Icon from '../../Icon';
import ChildsList from './ChildsList';

type Props = {
  children: string,
  openInitial?: boolean,
  items: Array<{ title: string, value: string, link?: string }>,
  selectedKey: string,
  onSelect?: (key: string) => void,
};

const Container: React.ComponentType<any> = styled.div``;
const NameBlock: React.ComponentType<any> = styled.div`
  position: relative;
`;

const calcRotate = (props: { rotAngle: number }) => `rotate(${props.rotAngle}deg)`;
const StyledIcon = styled(Icon)`
  position: absolute;
  top: 5px;
  left: 0;
  font-size: 20px;
  transform: ${calcRotate};
`;


const MenuItemsBlock = (props: Props) => {
  const { children, openInitial = false, ...rest } = props;

  const [open, setOpen] = React.useState(openInitial);

  const iconRotAngle = open ? 90 : 0;

  return (
    <Container>

      <NameBlock onClick={() => setOpen(!open)}>
        <MenuItem value="">{children}</MenuItem>
        <StyledIcon id="breadcrumbs" rotAngle={iconRotAngle} />
      </NameBlock>

      <Collapse isOpened={open}>
        <ChildsList {...rest} />
      </Collapse>

    </Container>
  );
};

export default React.memo<Props>(MenuItemsBlock);
