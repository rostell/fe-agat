// @flow

import * as React from 'react';
import styled from 'styled-components';

import Text from '../Text';

const Container: React.ComponentType<any> = styled.div`
  margin-bottom: 33px;
`;

type Props = {
  children: string,
};

const MenuHeader = (props: Props) => {
  const { children } = props;
  return (
    <Container>
      <Text size="title">{children}</Text>
    </Container>
  );
};

export default MenuHeader;
