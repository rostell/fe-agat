// @flow

import * as React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import Text from '../Text';
import * as appConfig from '../../../services/configService/appConfig';

type Props = {
  children: string,
  value: string,
  selected: boolean,
  level: number,
  link?: string,

  onSelect?: (key: string) => void,
};

const getColorBySelected = (selected: boolean, defaultColor: string) => selected
  ? appConfig.getColors().primary
  : defaultColor;

const calcPadding = (props: { level: number }) => `0px 24px 0px ${30 + props.level * 10}px`;
const calcMarginTop = (props: { level: number }) => props.level ? '8px' : '15px';
const calcBorderRightColor = (props: { selected: boolean }) => getColorBySelected(props.selected, 'transparent');

const Container: React.ComponentType<{ selected: boolean, level: number }> = styled.div`
  border-right: 2px solid transparent;
  cursor: pointer;
  margin-top: ${calcMarginTop};
  border-right-color: ${calcBorderRightColor};
`;

const StyledLink = styled(Link)`
  line-height: 32px;
  display: block;
  width: 100%;
  padding: ${calcPadding};
`;


const MenuItem = (props: Props) => {
  const { children, selected, level, value, link, onSelect } = props;

  const labelText = (
    <Text
      color={getColorBySelected(selected, 'black')}
      title={children}
      size={level ? 'small' : 'standard'}
    >
      {children}
    </Text>
  );

  return (
    <Container
      level={level}
      selected={selected}
      onClick={() => onSelect && onSelect(value)}
    >
      {link
        ? <StyledLink level={level} to={link}>{labelText}</StyledLink>
        : labelText
      }
    </Container>
  );
};

MenuItem.defaultProps = {
  selected: false,
  level: 0,
};

export default MenuItem;
