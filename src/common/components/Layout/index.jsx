// @flow

import { Layout as TlLayout } from '@telsystems/design';

const Layout = TlLayout;

export default Layout;

export { default as Header } from './Header';
export { default as Content } from './Content';
export { default as Footer } from './Footer';
export { default as Align } from './Align';
export { default as Right } from './Right';
export { default as Left } from './Left';
export { default as Sider } from './Sider';
