// @flow

import { Align as TlAlign } from '@telsystems/design';

const Align = TlAlign;

export default Align;
