// @flow

import * as React from 'react';

import styled from 'styled-components';

type Size = 'standard' | 'title' | 'small';
export type Props = {
  children: string,
  size?: Size | string,
  color?: string,
};

type ContainerProps = {
  size: Size | string,
  color: string,
};

const getFontSizeByName = (name: Size | string): string => {
  switch (name) {
    case 'standard': return '14px';
    case 'title': return '20px';
    case 'small': return '12px';
    default: return name;
  }
};

const Container: React.ComponentType<ContainerProps> = styled.span`
  font-size: ${({ size }) => getFontSizeByName(size)};
  color: ${({ color }) => color}
`;

const Text = (props: Props) => {
  const { children, ...rest } = props;
  return (
    <Container {...rest}>
      {children}
    </Container>
  );
};

Text.defaultProps = {
  size: 'standard',
  color: 'black',
};

export default Text;
