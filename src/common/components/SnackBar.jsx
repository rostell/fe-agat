// @flow

import { SnackBar as TlSnackBar } from '@telsystems/modals';

const SnackBar = TlSnackBar;

export default SnackBar;
