// @flow

import { IconButton as TlIconButton } from '@telsystems/inputs';

const IconButton = TlIconButton;

export default IconButton;
