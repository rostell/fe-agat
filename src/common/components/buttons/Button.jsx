// @flow

import { Button as TlButton } from '@telsystems/inputs';

const Button = TlButton;

export default Button;
