// @flow

import { DropDown as TlDropDown } from '@telsystems/inputs';

const DropDown = TlDropDown;

export default DropDown;
