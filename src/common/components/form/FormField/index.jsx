// @flow

import React, { type ComponentType } from 'react';
import styled from 'styled-components';
import { findIndex } from 'ramda';

import { type FieldConfig } from '../../../models/fieldsConfig';
import { type ObjectFieldValue } from '../../../models/objectData';
import Text from '../../Text';

import Tooltip from '../../Tooltip';

import TextInput from '../../inputs/TextInput';
import NumericInput from '../../inputs/NumericInput';
import TextareaInput from '../../inputs/TextareaInput';
import ToggleInput from '../../inputs/ToggleInput';
import SelectInput from '../../inputs/SelectInput';

const Container: ComponentType<any> = styled.div`
  padding: 0px 24px 16px 24px;
`;

const TitleContainer = styled.div`
  display: flex;
  margin-bottom: 4px;
`;

type Props = {
  index: number,
  fieldConfig: FieldConfig,
  value: ObjectFieldValue,
  errorText: ?string,
  onChange: (value: ObjectFieldValue) => void,
  onBlur: () => void,
};

const getSelectedIndex = (items, value) => {
  if (!items || !value) return undefined;
  const index = findIndex((item => item.content.id === value), items);
  return index === -1 ? undefined : index;
};

const renderInputControl = (
  fieldConfig: FieldConfig,
  value: ObjectFieldValue,
  errorText: ?string,
  onChange,
  onBlur,
) => {
  switch (fieldConfig.viewtype) {
    case 'select': return (
      <SelectInput
        selectedIndex={getSelectedIndex(fieldConfig.items, value)}
        items={fieldConfig.items || []}
        disabled={fieldConfig.readonly}
        fullWidth
        clearSelectedButton
        errorText={errorText}
        onItemClick={(v) => { onChange(v.id); onBlur(); }}
      />
    );
    case 'bit': return (
      <ToggleInput
        value={value}
        disabled={fieldConfig.readonly}
        onChange={(v) => { v !== value && onChange(v); onBlur(); }}
      />
    );
    case 'int': return (
      <NumericInput
        value={value}
        fullWidth
        disabled={fieldConfig.readonly}
        errorText={errorText}
        onChange={onChange}
        onBlur={onBlur}
      />
    );
    case 'text': return (
      <TextareaInput
        value={value}
        fullWidth
        disabled={fieldConfig.readonly}
        errorText={errorText}
        onChange={onChange}
        onBlur={onBlur}
      />
    );
    default: return (
      <TextInput
        value={value}
        fullWidth
        disabled={fieldConfig.readonly}
        errorText={errorText}
        onChange={onChange}
        onBlur={onBlur}
      />
    );
  }
};

const FormField = (props: Props) => {
  const { fieldConfig, value, errorText, index, onChange, onBlur } = props;
  const { title, description } = fieldConfig;
  return (
    <Container>

      <TitleContainer>
        <Text>{title}</Text>
        {Boolean(description) && (
          <Tooltip
            text={description}
            align="right"
            position={index ? 'top' : 'bottom'}
          />
        )}
      </TitleContainer>

      {renderInputControl(fieldConfig, value, errorText, onChange, onBlur)}
    </Container>
  );
};

export default FormField;
