// @flow

import React, { type ComponentType } from 'react';
import styled from 'styled-components';

import Text from '../Text';

const Container: ComponentType<any> = styled.div`
  padding: 16px 24px 16px 24px;
  border-top: 1px solid #eaeaf3;
`;

type Props = {
  id: string,
  title: string,
};

const FieldsSeparator = (props: Props) => {
  const { id, title } = props;
  return (
    <Container>
      <Text id={id} size="title">{title}</Text>
    </Container>
  );
};

export default FieldsSeparator;
