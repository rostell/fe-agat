// @flow

import React, { useImperativeHandle, forwardRef } from 'react';

import ReactScrollspy, { type ReactScrollspyProps } from 'react-scrollspy';

type Config = ?{
  scrollTo: (key: string) => void,
};

const Scrollspy = (props, ref) => {
  useImperativeHandle(ref, () => ({
    scrollTo: (id: string) => {
      const element: HTMLElement = global.document.getElementById(`${id}`);
      element.scrollIntoView({ block: 'start', behavior: 'smooth' });
    },
  }));

  return <ReactScrollspy {...props} />;
};

export default forwardRef<ReactScrollspyProps, Config>(Scrollspy);
