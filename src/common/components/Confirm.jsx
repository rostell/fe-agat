// @flow

import { Confirm as TlConfirm } from '@telsystems/modals';

const Confirm = TlConfirm;

export default Confirm;
