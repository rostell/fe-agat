// @flow

import { Select as TlSelect } from '@telsystems/inputs';

const SelectInput = TlSelect;

export default SelectInput;
