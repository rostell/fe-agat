// @flow
import React from 'react';

import { Toggle as TlToggle } from '@telsystems/inputs';

type Props = {
  value: boolean,
  onChange: (value: boolean) => void,
};

const ToggleInput = (props: Props) => {
  const { value, onChange } = props;
  return (
    <TlToggle
      checked={value}
      onChange={onChange}
    />
  );
};

export default ToggleInput;
