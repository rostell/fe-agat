// @flow

import { Textarea as TlTextarea } from '@telsystems/inputs';

const TextareaInput = TlTextarea;

export default TextareaInput;
