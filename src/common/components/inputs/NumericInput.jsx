// @flow

import { NumericInput as TlNumericInput } from '@telsystems/inputs';

const NumericInput = TlNumericInput;

export default NumericInput;
