// @flow

import { Icon as TlIcon } from '@telsystems/design';

const Icon = TlIcon;

export default Icon;
