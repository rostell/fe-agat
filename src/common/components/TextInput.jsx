// @flow

import { TextInput as TlTextInput } from '@telsystems/inputs';

const TextInput = TlTextInput;

export default TextInput;
