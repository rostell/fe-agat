import React from 'react';
import { ConnectedRouter } from 'connected-react-router';

import * as navigationService from '../services/navigatonService';

import Routes from './Routes';

const RootRouter = () => (
  <ConnectedRouter history={navigationService.getHistoryInstance()}>
    <Routes />
  </ConnectedRouter>
);


export default RootRouter;
