// @flow

import React, { Fragment } from 'react';
import { Route, Redirect } from 'react-router';

import { appConfig } from '../services/configService';

import MainPage from '../pages/MainPage';

import { basePath } from '../common/consts';


const Routes = () => (
  <Fragment>
    <Route path={`${basePath}/`} component={MainPage} />
    <Route
      exact
      path={`${basePath}/`}
      render={() => (
        <Redirect to={`${basePath}/${appConfig.getDefaultObjectsSectionKey()}`} />
      )}
    />
  </Fragment>
);

export default Routes;
