import React from 'react';
import ReactDOM from 'react-dom';

import '@telsystems/design/dist/main.css';
import '@telsystems/inputs/dist/main.css';
import '@telsystems/table/dist/main.css';
import '@telsystems/modals/dist/main.css';

import { appConfig } from './services/configService';

(async () => {
  await appConfig.init();
  const { default: App } = await import('./App');
  ReactDOM.render(<App />, global.document.getElementById('root'));
})();
