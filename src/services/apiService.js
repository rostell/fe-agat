import axios from 'axios';

import { appConfig } from './configService';
import { parseFromApi } from '../common/models/formConfig';
import { basePath } from '../common/consts';
import { getHistoryInstance } from './navigatonService';

const baseUrl = '';

export const loadAppMeta = async () => {
  const response = await axios.get(`${basePath}/appMeta.json`);
  return response.data;
};


const genUrlFromTemplate = (template, data) => {
  const params = template.match(/%([\w\.]+)%/g);
  if (!params) return template;
  const paramsNames = params.map(paramName => paramName.slice(1, paramName.length - 1));
  return paramsNames.reduce((acc, paramName) => acc.replace(`%${paramName}%`, data[paramName]), template);
};

const makeRequest = async (params) => {
  const {
    method = 'get',
    url,
  } = params;
  const response = await axios[method](`${baseUrl}/${url}`);

  if (!response.data || response.data.resultcode) {
    if (response.data.resultcode === 1407) {
      getHistoryInstance().push(appConfig.getNoCookieLink());
    } else {
      throw new Error(response.data.resultmsg);
    }
  }

  return response.data.data;
};


export const loadGridItems = url => makeRequest({ url });


export const loadFormConfig = async (url, blockName) => {
  const data = await makeRequest({ url });
  return parseFromApi(data[blockName]);
};

export const loadObjectForEdit = async (urlTemplate, id) => {
  const url = urlTemplate.replace('%id%', id);
  return makeRequest({ url });
};

export const saveObject = async (urlTemplate, objectData) => {
  const url = genUrlFromTemplate(urlTemplate, objectData);
  return makeRequest({ method: 'post', url });
};

export const deleteObjects = async (urlTemplate, objectsData) => {
  const deletionPromises = objectsData.map((objectData) => {
    const url = genUrlFromTemplate(urlTemplate, objectData);
    return makeRequest({ method: 'post', url });
  });

  await Promise.all(deletionPromises);
};

export const loadReferences = async (referenceConfig, objectData) => {
  const { api, title, value } = referenceConfig;
  const url = genUrlFromTemplate(api, objectData);
  const items = await makeRequest({ url });
  return items.map(item => ({ id: item[value], title: item[title] }));
};
