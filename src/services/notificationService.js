import uuid from 'uuid/v1';

let handler = null;

export const setHandler = (h) => { handler = h; };

export const showError = ({ message }) => handler({
  id: uuid(),
  type: 'error',
  text: message,
});

export const showInfoText = text => handler({
  id: uuid(),
  type: 'info',
  text,
});
