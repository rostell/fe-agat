import { createBrowserHistory } from 'history';

const history = createBrowserHistory();

export const getHistoryInstance = () => history;
