// @flow

import { loadAppMeta } from '../apiService';

import { genObjectsSectionsFromConfig, type ObjectsSection } from '../../common/models/objectsSection';
import { genColorsFromConfig, type Colors } from '../../common/models/colors';

let appMeta = null;

export const init = async () => {
  appMeta = await loadAppMeta();
};

export const getTitle = (): string => appMeta.app_title;
export const getNoCookieLink = (): string => appMeta.general.no_cookie;
export const getLogoPath = (): string => '/api/resource/v1/logo/get';
export const getDefaultObjectsSectionKey = (): string => appMeta.general.default_object;
export const getLogoLink = (): string => appMeta.general.logo_click;
export const getColors = (): Colors => genColorsFromConfig(appMeta.general.colours);

export const getObjectsConfigs = (): Array<ObjectsSection> => appMeta.objects
  .map(genObjectsSectionsFromConfig);

export const getObjectConfigById = (key: string) => getObjectsConfigs()
  .find(obj => obj.key === key);
