// @flow

import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { appConfig } from '../../services/configService';

import { actions as gridActions } from '../../store/actions/grid';
import { actions as objectFormActions } from '../../store/actions/objectForm';

import { IconButton, Button } from '../../common/components/buttons';


const Container = styled.div`
  display: flex;
`;

const Spacer = styled.div`
  width: 16px;
`;

type Props = {
  selectedObjectsCount: number,
  selectedSectionKey: string,
  onCreateNewObject: () => void,
  onDeleteSelected: () => void,
}

const ActionBtnsBlock = (props: Props) => {
  const { selectedObjectsCount, selectedSectionKey, onDeleteSelected, onCreateNewObject } = props;
  const objectsSectionConfig = appConfig.getObjectConfigById(selectedSectionKey);
  const { actions } = objectsSectionConfig;
  return (
    <Container>
      {Boolean(selectedObjectsCount) && !actions.delete.isHidden && (
        <IconButton iconId="trash" onClick={onDeleteSelected}>
          {`${actions.delete.title} (${selectedObjectsCount})`}
        </IconButton>
      )}

      <Spacer />

      {!actions.create.isHidden && (
        <IconButton onClick={onCreateNewObject} iconId="plus">
          {actions.create.title}
        </IconButton>
      )}

      <Spacer />

      <Button type="secondary">
        Импорт/Экспорт
      </Button>
    </Container>
  );
};

const mapStateToProps = ({ grid, objectsSections }) => ({
  selectedObjectsCount: grid.selectedIndexes.length,
  selectedSectionKey: objectsSections.selected,
});

const mapDispatchToProps = {
  onDeleteSelected: gridActions.deleteSelectedItems,
  onCreateNewObject: objectFormActions.createNewObject,
};

export default connect(mapStateToProps, mapDispatchToProps)(ActionBtnsBlock);
