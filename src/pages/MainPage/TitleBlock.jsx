// @flow

import React, { type ComponentType } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import { appConfig } from '../../services/configService';

import Text from '../../common/components/Text';
import Icon from '../../common/components/Icon';

const Container: ComponentType<any> = styled.div`
  margin: 32px 40px 40px;
  display: flex;
  align-items: center;
`;

const LogoContainer = styled.div`
  height: 24px;
`;
const IconDelimiter = styled(Icon)`
  font-size: 22px;
  margin: 0px 8px;
`;

const LogoImg = styled.img`
  width: 150px;
`;

const appTitle = appConfig.getTitle();
const logLink = appConfig.getLogoLink();
const logoPath = appConfig.getLogoPath();

const TitleBlock = () => {
  return (
    <Container>
      <LogoContainer>
        <Link to={logLink}>
          <LogoImg src={logoPath} alt="logo" />
        </Link>
      </LogoContainer>
      <IconDelimiter id="breadcrumbs" />
      <Text size="title">{appTitle}</Text>
    </Container>
  );
};

export default TitleBlock;
