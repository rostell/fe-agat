import React from 'react';

import styled from 'styled-components';
import { Align, Right, Left } from '@telsystems/design';

import GridFilter from '../../common/blocks/GridFilter';
import ActionBtnBlock from './ActionBtnsBlock';

const Container = styled.div`
  margin-bottom: 25px;
`;

const FilterAndActions = () => {
  return (
    <Container>
      <Align>
        <Left>
          <GridFilter />
        </Left>
        <Right>
          <ActionBtnBlock />
        </Right>
      </Align>
    </Container>
  );
};

export default FilterAndActions;
