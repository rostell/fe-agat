// @flow

import React, { type ComponentType } from 'react';
import styled from 'styled-components';

import ObjectsSectionsMenu from '../../common/blocks/ObjectsSectionsMenu';
import ObjectsTable from '../../common/blocks/ObjectsTable';
import ConfirmationDialog from '../../common/blocks/ConfirmationDialog';

import ObjectForm from '../../common/blocks/ObjectForm';
import Notification from '../../common/blocks/Notification';

import TitleBlock from './TitleBlock';
import FilterAndActions from './FilterAndActions';


const Container: ComponentType<any> = styled.div`
  display: flex;
  height: 100%;
`;

const SideMenuContainer = styled.div`
  background-color: #f9f9fb;
  min-width: 256px;
`;

const ContentContainer = styled.div`
  flex: 1;
`;

const GridContainer = styled.div`
  padding: 0px 80px 0px 80px;
`;

const MainPage = () => (
  <Container>
    <SideMenuContainer>
      <ObjectsSectionsMenu />
    </SideMenuContainer>

    <ContentContainer>
      <TitleBlock />
      <GridContainer>
        <FilterAndActions />
        <ObjectsTable />
      </GridContainer>
    </ContentContainer>

    <Notification />
    <ConfirmationDialog />
    <ObjectForm />
  </Container>
);


export default MainPage;
