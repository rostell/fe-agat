// @flow

declare module 'styled-components' {
  // { type: '__styled' } is really only used ot avoid type collisions
  // betwteen the interpolation unions types of (props: P) => string | number)
  // and StyledComponent<any>.
  declare export type StyledComponent<Props> = { type: '__styled' } & React$ComponentType<Props>;

  declare export type Interpolation<P> =
    | StyledComponent<any> // Order seems to matter unexpectedly.
    | (P => string | number)
    | string
    | number;

  declare type TaggedTemplateLiteralBound<Props> = (strings: Array<string>, ...args: Array<Interpolation<Props>>) => StyledComponent<Props>
  declare type TaggedTemplateLiteral = <Props>(strings: Array<string>, ...args: Array<Interpolation<Props>>) => StyledComponent<Props>;

  declare type TagTemplateCreatorBound<P> = {
    [[call]]: TaggedTemplateLiteralBound<P>,
    attrs<P>({
      style?: (p: P) => { [property: string]: number | string },
      [property: string]: ((p: P) => string) | string
    }): TagTemplateCreatorBound<P>
  };

  declare type TagTemplateCreator = {
    [[call]]: TaggedTemplateLiteral,
    attrs<P>({
      style?: (p: P) => { [property: string]: number | string },
      [property: string]: ((p: P) => string) | string
    }): TagTemplateCreatorBound<P>
  }

  declare type StyledComponentList = {
    a:                        TagTemplateCreator,
    abbr:                     TagTemplateCreator,
    address:                  TagTemplateCreator,
    area:                     TagTemplateCreator,
    article:                  TagTemplateCreator,
    aside:                    TagTemplateCreator,
    audio:                    TagTemplateCreator,
    b:                        TagTemplateCreator,
    base:                     TagTemplateCreator,
    bdi:                      TagTemplateCreator,
    bdo:                      TagTemplateCreator,
    big:                      TagTemplateCreator,
    blockquote:               TagTemplateCreator,
    body:                     TagTemplateCreator,
    br:                       TagTemplateCreator,
    button:                   TagTemplateCreator,
    canvas:                   TagTemplateCreator,
    caption:                  TagTemplateCreator,
    cite:                     TagTemplateCreator,
    code:                     TagTemplateCreator,
    col:                      TagTemplateCreator,
    colgroup:                 TagTemplateCreator,
    data:                     TagTemplateCreator,
    datalist:                 TagTemplateCreator,
    dd:                       TagTemplateCreator,
    del:                      TagTemplateCreator,
    details:                  TagTemplateCreator,
    dfn:                      TagTemplateCreator,
    dialog:                   TagTemplateCreator,
    div:                      TagTemplateCreator,
    dl:                       TagTemplateCreator,
    dt:                       TagTemplateCreator,
    em:                       TagTemplateCreator,
    embed:                    TagTemplateCreator,
    fieldset:                 TagTemplateCreator,
    figcaption:               TagTemplateCreator,
    figure:                   TagTemplateCreator,
    footer:                   TagTemplateCreator,
    form:                     TagTemplateCreator,
    h1:                       TagTemplateCreator,
    h2:                       TagTemplateCreator,
    h3:                       TagTemplateCreator,
    h4:                       TagTemplateCreator,
    h5:                       TagTemplateCreator,
    h6:                       TagTemplateCreator,
    head:                     TagTemplateCreator,
    header:                   TagTemplateCreator,
    hgroup:                   TagTemplateCreator,
    hr:                       TagTemplateCreator,
    html:                     TagTemplateCreator,
    i:                        TagTemplateCreator,
    iframe:                   TagTemplateCreator,
    img:                      TagTemplateCreator,
    input:                    TagTemplateCreator,
    ins:                      TagTemplateCreator,
    kbd:                      TagTemplateCreator,
    keygen:                   TagTemplateCreator,
    label:                    TagTemplateCreator,
    legend:                   TagTemplateCreator,
    li:                       TagTemplateCreator,
    link:                     TagTemplateCreator,
    main:                     TagTemplateCreator,
    map:                      TagTemplateCreator,
    mark:                     TagTemplateCreator,
    menu:                     TagTemplateCreator,
    menuitem:                 TagTemplateCreator,
    meta:                     TagTemplateCreator,
    meter:                    TagTemplateCreator,
    nav:                      TagTemplateCreator,
    noscript:                 TagTemplateCreator,
    object:                   TagTemplateCreator,
    ol:                       TagTemplateCreator,
    optgroup:                 TagTemplateCreator,
    option:                   TagTemplateCreator,
    output:                   TagTemplateCreator,
    p:                        TagTemplateCreator,
    param:                    TagTemplateCreator,
    picture:                  TagTemplateCreator,
    pre:                      TagTemplateCreator,
    progress:                 TagTemplateCreator,
    q:                        TagTemplateCreator,
    rp:                       TagTemplateCreator,
    rt:                       TagTemplateCreator,
    ruby:                     TagTemplateCreator,
    s:                        TagTemplateCreator,
    samp:                     TagTemplateCreator,
    script:                   TagTemplateCreator,
    section:                  TagTemplateCreator,
    select:                   TagTemplateCreator,
    small:                    TagTemplateCreator,
    source:                   TagTemplateCreator,
    span:                     TagTemplateCreator,
    strong:                   TagTemplateCreator,
    style:                    TagTemplateCreator,
    sub:                      TagTemplateCreator,
    summary:                  TagTemplateCreator,
    sup:                      TagTemplateCreator,
    table:                    TagTemplateCreator,
    tbody:                    TagTemplateCreator,
    td:                       TagTemplateCreator,
    textarea:                 TagTemplateCreator,
    tfoot:                    TagTemplateCreator,
    th:                       TagTemplateCreator,
    thead:                    TagTemplateCreator,
    time:                     TagTemplateCreator,
    title:                    TagTemplateCreator,
    tr:                       TagTemplateCreator,
    track:                    TagTemplateCreator,
    u:                        TagTemplateCreator,
    ul:                       TagTemplateCreator,
    var:                      TagTemplateCreator,
    video:                    TagTemplateCreator,
    wbr:                      TagTemplateCreator,
    // SVG
    circle:                   TagTemplateCreator,
    clipPath:                 TagTemplateCreator,
    defs:                     TagTemplateCreator,
    ellipse:                  TagTemplateCreator,
    g:                        TagTemplateCreator,
    image:                    TagTemplateCreator,
    line:                     TagTemplateCreator,
    linearGradient:           TagTemplateCreator,
    mask:                     TagTemplateCreator,
    path:                     TagTemplateCreator,
    pattern:                  TagTemplateCreator,
    polygon:                  TagTemplateCreator,
    polyline:                 TagTemplateCreator,
    radialGradient:           TagTemplateCreator,
    rect:                     TagTemplateCreator,
    stop:                     TagTemplateCreator,
    svg:                      TagTemplateCreator,
    text:                     TagTemplateCreator,
    tspan:                    TagTemplateCreator,
  };

  declare export default StyledComponentList & {
    [[call]]: <S : string>(S) => $ElementType<StyledComponentList, S>,
    [[call]]: <Props : {}>(React$ComponentType<Props>) => TaggedTemplateLiteralBound<Props>
  };
}