// @flow

import { Component } from 'react';

declare module '@telsystems/inputs' {
  declare type ItemContent = { id: string, title: string };

  declare export class TextInput extends React$Component<{
    value: string,
    placeholder?: string,
    disabled?: boolean,
    onChange?: (value: string) => void,
  }> {}

  declare export class Textarea extends React$Component<{
    value: string,
    placeholder?: string,
    disabled?: boolean,
    onChange?: (value: string) => void,
  }> {}

  declare export class NumericInput extends React$Component<{
    value: number,
    placeholder?: string,
    disabled?: boolean,
    onChange?: (value: number) => void,
  }> {}

  declare export class Toggle extends React$Component<{
    checked: boolean,
    disabled?: boolean,
    onChange?: (checked: boolean) => void,
  }> {}

  declare export class Select extends React$Component<{
    selectedIndex: ?number,
    placeholder?: string,
    items: Array<{ content: ItemContent }>,
    disabled?: boolean,
    onChange?: (index: number) => void,
  }> {}


  declare export class DropDown extends React$Component<{
    children: Component<any>,
    items: Array<{ content: ItemContent, selected?: boolean }>,
    onItemClick: (itemContent: ItemContent, index: number) => void,
  }> {}

  declare export class Button extends React$Component<{
    children: string,
    type?: 'primary' | 'secondary',
    onClick?: (value: string) => void,
  }> {}

  declare export class IconButton extends React$Component<{
    children: string,
    iconId: string,
    type?: 'primary' | 'secondary',
    onClick?: (value: string) => void,
  }> {}
}
