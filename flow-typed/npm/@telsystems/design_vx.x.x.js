// @flow

declare module '@telsystems/design' {
  declare export class Icon extends React$Component<{
    id: string,
  }> {}

  declare export class Layout extends React$Component<{}> {}

  declare export class Sider extends React$Component<{}> {}

  declare export class Header extends React$Component<{}> {}

  declare export class Content extends React$Component<{}> {}

  declare export class Footer extends React$Component<{}> {}

  declare export class Align extends React$Component<{}> {}

  declare export class Right extends React$Component<{}> {}

  declare export class Left extends React$Component<{}> {}

  declare export class Tooltip extends React$Component<{
    size?: string,
    position?: string,
    header?: string,
    text: string,
    link?: string,
    link_text?: string,
    align?: string,
    isActive?: boolean,
  }> {}
}
