// @flow
import { type Node } from 'react';

declare module '@telsystems/modals' {
  declare export class Dialog extends React$Component<{
    children: Node,
    show: boolean,
    modal?: boolean,
    type?: string,
    closable?: boolean,
    onCloseClick?: () => void,
  }> {}

  declare export class Confirm extends React$Component<{
    show: boolean,
    modal?: boolean,
    type?: string,
    onCloseClick: () => void,
    onConfirmClick: () => void,
  }> {}

  declare export class SnackBar extends React$Component<{
    show: boolean,
    text: string,
    onCloseClick: () => void,
  }> {}
}
