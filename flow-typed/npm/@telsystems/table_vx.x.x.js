// @flow

declare module '@telsystems/table' {
  declare export type TableSorting = { id: string, direction: 'asc' | 'des' }
  declare export class TableSuperDraggable extends React$Component<{}> {
    orientation: string,
    align: string,
    scheme: Array<Object>,
    records: Array<Object>,
    selected: Array<number>,
    sortedBy: TableSorting,
    onChoiceColumn: () => void,
    // onClick: (id: string) => void,
    showPlaceholder: boolean,
    isWaiting: boolean,
  }
}
