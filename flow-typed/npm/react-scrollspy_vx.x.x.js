// @flow

import { type Node } from 'react';

declare module 'react-scrollspy' {
  declare export type ReactScrollspyProps = {
    children: Node,
    items: Array<string>,
    rootEl: string,
    onUpdate: (el: HTMLElement) => void,
  };

  declare export default class ReactScrollspy extends React$Component<ReactScrollspyProps> {}
}
