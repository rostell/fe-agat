// @flow

// import * as React from 'react';

declare module 'react-collapse' {
  // declare type Collapse = React.Component<{ isOpened: boolean }>
  // declare export default d;

  declare export default class Collapse extends React$Component<{
    isOpened: boolean,
    children?: React$Node
  }> {}
}
