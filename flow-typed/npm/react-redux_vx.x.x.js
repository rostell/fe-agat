// @flow
import { Component } from 'react';

declare module 'react-redux' {
  declare function connected(Component): Component;
  declare export function connect(mapStateToProps: any, mapDispatchToProps: any): typeof connected;
}
